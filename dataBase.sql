DROP DATABASE IF EXISTS tienda;
CREATE DATABASE IF NOT EXISTS tienda;
USE tienda;

-- Tablas Primarias

CREATE TABLE rol(
idRol INT AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(15) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE categoria(
idCategoria INT AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(25) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE producto(
idProducto INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
idCategoria INT NOT NULL,
nombre VARCHAR(40) NOT NULL,
precio INT NOT NULL,
fechaVencimiento DATE NOT NULL,
detalle TEXT,
FOREIGN KEY(idCategoria) REFERENCES categoria(idCategoria) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE usuario(
idUsuario INT NOT NULL AUTO_INCREMENT PRIMARY KEY,  
idRol INT NOT NULL,
ci VARCHAR(12) UNIQUE NOT NULL,
primerNombre VARCHAR(15) NOT NULL,
segundoNombre VARCHAR(15),
apellidoPaterno VARCHAR(15) NOT NULL,
apellidoMaterno VARCHAR(15),
fechaNacimiento DATE NOT NULL,
celular INT NOT NULL,
usuario VARCHAR(20) UNIQUE NOT NULL,
contrasenia VARCHAR(20) UNIQUE NOT NULL,
FOREIGN KEY(idRol) REFERENCES rol(idRol) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;


CREATE TABLE venta(
idVenta INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
idUsuario INT NOT NULL,
idCliente INT NOT NULL,
cantidadVenta INT NOT NULL,
precioTotal FLOAT NOT NULL,
FOREIGN KEY(idUsuario) REFERENCES usuario(idUsuario) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE detalleVenta(
idVenta INT NOT NULL,
idProducto INT NOT NULL,
nombreProducto VARCHAR(40) NOT NULL,
precioUnidad INT NOT NULL,
cantidadProductoUnidad INT NOT NULL, 
FOREIGN KEY(idVenta) REFERENCES venta(idVenta) ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY(idProducto) REFERENCES producto(idProducto) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

-- CREATE TABLE factura(
-- idFactura INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
-- idPedido INT NOT NULL UNIQUE,
-- numeroFactura INT NOT NULL UNIQUE,
-- fechaHoraFacturacion DATETIME NOT NULL,
-- codigoControl VARCHAR(15) NOT NULL,
-- fechaLimiteEmision DATE NOT NULL,
-- codigoQR BLOB,
-- FOREIGN KEY(idPedido) REFERENCES pedido(idPedido) ON UPDATE CASCADE ON DELETE CASCADE
-- )ENGINE=InnoDB;

-- CONSULTA PRODUCTOS
SELECT c.idCategoria, p.nombre, p.precio, p.fechaVencimiento, p.detalle
FROM categoria c INNER JOIN producto p ON c.idCategoria = p.idCategoria;


--CONSULTA USUARIOS
SELECT u.idUsuario, r.nombre, u.ci, CONCAT_WS(' ', u.primerNombre, u.segundoNombre,u.apellidoPaterno,u.apellidoMaterno), u.celular
FROM rol r INNER JOIN usuario u ON r.idRol = u.idRol;

--CONSULTA USUARIOS RENOMBRADO
SELECT u.idUsuario AS ID, r.nombre AS Rol, u.ci AS CI, CONCAT_WS(' ', u.primerNombre, u.segundoNombre,u.apellidoPaterno,u.apellidoMaterno) AS Nombre
FROM rol r INNER JOIN usuario u ON r.idRol = u.idRol;